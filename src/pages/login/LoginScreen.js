import { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import logo_teleapp from '../../assetss/img/logo-TELAPP.png';
import { AuthContext } from '../../auth/authContext';
import { types } from '../../types/types';


export const LoginScreen = () => {

    const navigate = useNavigate();
    const { dispatch } = useContext( AuthContext );

    const handleLogin = () => {
        const action = {
            type: types.login,
            payload: { name: 'Bartolo Garcia Leyva' }
        }

        dispatch(action);

        const lastPath = localStorage.getItem('lastPath') || '/programarSalida';

        navigate(lastPath, {
            replace: true
        });
    }

    // const handleLogin = () => {
    //     navigate('/programarSalida', {
    //         replace: true
    //     });
    // }

    return (
        <>
            <div className="login-bg">
                <div className="container">
                    <div className="col-sm-12 col-12 ">
                        <div className="login-card animated fadeInUp delay-02s">
                            <figure> <img src={logo_teleapp} className="mt-5 mb-5 mx-auto d-block animated fadeInDown delay-03s" alt="Logo-GPS" width="200px" height="auto"/> </figure>
                            <form>
                                <div className="form-group m-t-40"> 
                                    <input type="text" className="form-control" name="numEmpleado" placeholder="Número de empleado" /> 
                                </div> 
                                <div className="form-group pass_show m-t-40"> 
                                    <input type="password" className="form-control" name="password" placeholder="Contraseña" /> 
                                </div> 
                                <button onClick={ handleLogin } type="button" className="m-t-40 btn btn-warning btn-lg btn-block animated fadeInUp delay-06s">ENTRAR</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
