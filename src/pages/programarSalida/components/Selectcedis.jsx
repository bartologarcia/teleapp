import React from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';
// import css
import '../../../assetss/css/programarSalida.css';


export const Selectcedis = () => {
    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col-6">
                        <select defaultValue={'DEFAULT'} className="form-select selectCedis" aria-label="Default select example">
                            <option value="DEFAULT" disabled>Open this select menu</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    <div className="col-6">
                        <select defaultValue={'DEFAULT'} className="form-select selectCedis" aria-label="Default select example">
                            <option value="DEFAULT" disabled>Open this select menu</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    <div className="input-group mb-3 buscador">
                        <input type="text" className="form-control buscar-unidad" placeholder="BUSCAR UNIDAD" aria-label="Search" />
                        <span className="input-group-text search" id="basic-addon1">
                            <FontAwesomeIcon icon={ faMagnifyingGlass } />
                        </span>
                    </div>
                </div>
            </div>
        </div>
    )
}