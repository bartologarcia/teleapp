import React, { useContext } from "react";
import { NavLink, useNavigate } from 'react-router-dom';
import { AuthContext } from "../../auth/authContext";
import { types } from "../../types/types";
// iport icons
import pictograma from '../../assetss/img/pictograma.svg';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faChevronRight, faPowerOff, faBars} from '@fortawesome/free-solid-svg-icons';




export const Navbar = () => {

    const { user, dispatch } = useContext(AuthContext);

    const navigate = useNavigate();

    const handleLogout = () => {

        dispatch({
            type: types.logout
        });

        navigate('/login', {
            replace: true
        });
    }
    

    const ClikMenu = () => {
        const body = document.querySelector("body");
        const logo = document.querySelector("#menu");
        const iconMenu = document.querySelector("#icon-menu");
        const sombra_menu = document.querySelector("#vertical-menu-wrapper");

        // Agregar y ocultar menu
        if(document.querySelector(".closed-menu")){
            body.classList.remove('closed-menu');
        } else {
            body.classList.add('closed-menu');
        }	
        // */Finaliza

        // Agregar y quita estilos de css al menu 
        if(document.querySelector(".ocultar-menu")){
            logo.classList.remove('ocultar-menu');
            iconMenu.classList.remove('pocicionar-icono-menu');
            iconMenu.classList.add('padding-menu');
        } else {
            logo.classList.add('ocultar-menu');
            iconMenu.classList.add('pocicionar-icono-menu');
            iconMenu.classList.remove('padding-menu');
        }	

        if(document.querySelector(".vertical-menu-wrapper-add")){
            sombra_menu.classList.remove('vertical-menu-wrapper-add');
            sombra_menu.classList.add('move-menu');
        } else {
            sombra_menu.classList.add('vertical-menu-wrapper-add');
            sombra_menu.classList.remove('move-menu');
        }

        // */Finaliza
    }

    return (
        <>
            <div className="text-header header-menu"> <h5> CEDIS XXX | UNIDADES EN RUTA </h5></div>
            <nav id="vertical-menu-wrapper" className="vertical-menu-wrapper vertical-menu-wrapper-add">
                <div className="vertical-menu-logo">
                    <div className="sidebar-header text-center">
                        <img id="menu" src={pictograma} alt="logo menu" width="39px"/>
                    </div>
                    <span onClick={ClikMenu} id="icon-menu" className="open-menu-btn padding-menu"><FontAwesomeIcon icon={faBars} className="pull-right" /> </span>
                </div>
                <ul className="vertical-menu">
                    <li id="NameUser">{ user.name }</li>
                    <small className="white">CONTROL DE SEGURIDAD (CEDIS)</small>
                    <hr />
                    <li>
                        <NavLink to="/programarSalida"> PROGRAMAR SALIDA <FontAwesomeIcon icon={faChevronRight} className="pull-right f12" /> </NavLink>
                    </li>
                    <li>
                        <NavLink to="/trafico"> TRÁFICO <FontAwesomeIcon icon={faChevronRight} className="pull-right f12" /> </NavLink>
                    </li>
                    <li>
                        <NavLink to="/enRuta"> EN RUTA <FontAwesomeIcon icon={faChevronRight} className="pull-right f12" /> </NavLink>
                    </li>
                    <li>
                        <NavLink to="/validacion"> VALIDACIÓN <FontAwesomeIcon icon={faChevronRight} className="pull-right f12" /> </NavLink>
                    </li>
                    <li>
                        <a onClick={handleLogout} className="cerrarSesion"> CERRAR SESIÓN <FontAwesomeIcon icon={faPowerOff} className="pull-right f12" /> </a>
                    </li>
                </ul>
            </nav>
            {/* <div className="content-wrapper">
                <div className="content">
                </div>
            </div> */}
        
        </>
    )
}
