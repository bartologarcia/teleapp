// import librerias
import React from "react";
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import { LoginScreen } from '../pages/login/LoginScreen';
import { DashboardRoutes } from './DashboardRoutes';
import { PrivateRoute } from "./PrivateRoute";
import { PublicRoute } from "./PublicRoute";
// import css
import '../assetss/css/App.css';
import '../assetss/css/gps.css';
import 'bootstrap/dist/css/bootstrap.css';




export const AppRouter = () => {
  return (
    <BrowserRouter>    
      <Routes>
          
        <Route path="/login" element={
          <PublicRoute>
            <LoginScreen />
          </PublicRoute>
          } 
        />
        
        <Route path="/*" element={ 
            <PrivateRoute>
                <DashboardRoutes />
            </PrivateRoute >  
          } 
        />
        {/* <Route path="/*" element={ <DashboardRoutes />  } /> */}

      </Routes>
    </BrowserRouter>
  )
}
