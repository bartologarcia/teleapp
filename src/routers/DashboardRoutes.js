import { Routes, Route } from 'react-router-dom';
import { Navbar } from '../pages/ui/NavBar';
import { ProgramarSalida } from '../pages/programarSalida/ProgramarSalida';
import { Trafico } from '../pages/trafico/Trafico';
import { EnRuta } from '../pages/enRuta/EnRuta';
import { Validacion } from '../pages/validacion/Validacion';


export const DashboardRoutes = () => {
    return (
        <>
            <Navbar />

            <div className="container">
                <Routes>

                    <Route exact path="programarSalida" element={ <ProgramarSalida/>} />
                    <Route exact path="trafico" element={ <Trafico/>} />
                    <Route exact path="enRuta" element={ <EnRuta/>} />
                    <Route exact path="validacion" element={ <Validacion/> } />

                </Routes>
            </div>
        </>
    )
}
